class point(var x:Int,var y:Int)
// toString მეთოდი
fun main() {
    var point_result = point(25,12)
    var coordinate_x = point_result.x
    var coordinate_y = point_result.y

    fun q1(coordinate_x:Int,coordinate_y:Int){
        println(coordinate_x.toString())
        println(coordinate_y.toString())
    }
    q1(25,27)
    // კორდინატების შედარება
    fun q2(coordinate_x:Int,coordinate_y:Int){
        if (coordinate_x > coordinate_y) {
            print("x cord is greater")
        }
        else if (coordinate_y > coordinate_x) {
            print("y cord is greater")
        }
        else {
            println("x and y are equal")
        }
    }
    q2(5,5)
    // სიმეტრია
    fun question3(coordinate_x:Int,coordinate_y:Int) {
        if (coordinate_x > 0 && coordinate_y > 0) {
            var coordinate_x = coordinate_x - coordinate_x * 2
            var coordinate_y = coordinate_y - coordinate_y * 2
            println(coordinate_x)
            println(coordinate_y)
        }
        if (coordinate_x < 0 && coordinate_y < 0) {
            var coordinate_x = coordinate_x * -1
            var coordinate_y = coordinate_y * -1

            println(coordinate_x)
            println(coordinate_y)
        }


    }
    question3(25,-2)

    // ორ წერტილს შორის მანძილი

    fun q4(coordinate_x:Int,coordinate_y:Int) {

        var square_cord_x = coordinate_x * coordinate_x
        var square_cord_y = coordinate_y * coordinate_y

        var sum = square_cord_x + square_cord_y
        var distance = Math.sqrt(sum.toDouble())

        println(distance)


    }


    q4(3,4)


    class fraction {
        var numerator: Double = 2.3
        var denominator: Double = 4.6
    }
    var fract = fraction()
    var num1 = fract.numerator
    var num2 = fract.denominator
    // შეკრება
    fun sum(num1:Int,num2:Int) {
        println(num1 + num2)
    }
    sum(5,8)
    // გამრავლებაა
    fun mult(num1:Int,num2:Int) {
        println(num1 * num2 )
    }
    mult(2,10)
}
